import sys
import json

from PyQt5 import QtSerialPort
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.uic import loadUi

from layout import Ui_MainWindow
from splash_screen import Ui_SplashScreen
from database import DatabaseFunctions

counter = 0

class WeatherStation(QMainWindow, Ui_MainWindow):
    def __init__(self):

        super(WeatherStation, self).__init__()
        loadUi('new_dashboard_1.ui', self)

        # Initialization Setup
        self.MainNavigator.setCurrentIndex(0)
        self.error = QErrorMessage()
        try:
            config = open('config.json', 'r')
            config = json.loads(config.read())
            self.comPort = config["com_port"]
            print(self.comPort)
            if(len(self.comPort) < 1):
                self.MainNavigator.setCurrentIndex(2)
            self.comPortText.setText(str(self.comPort))

        except Exception as e:

            self.error.showMessage(str(e))
            self.error.exec_()
            self.close()
        # Serial Port connection
        self.serial = QtSerialPort.QSerialPort(
            str(self.comPort),
            baudRate=QtSerialPort.QSerialPort.Baud9600,
            readyRead=self.startReading
        )

        if (len(self.comPort) > 0):
            self.startReading()

        self.temperatureLabel.setText("0")
        self.windLabel.setText("0")
        self.windDirection.setText("0")
        self.humidityLabel.setText("0")
        self.rainfallLabel.setText("0")
        self.solarLabel.setText("0")
        self.soilMoistureLabel.setText("0")
        self.barometerLabel.setText("0")
        self.soilTempLabel.setText("0")
        self.leafLabel.setText("0")

        self.ReportsTable.setColumnCount(10)
        self.TableHeaderLabel = ["Wind Speed",
                                 "Wind Direction",
                                 "Air Temperature",
                                 "Humidity",
                                 "Rainfall",
                                 "Solar Radiation",
                                 "Barometric Pressure",
                                 "Soil Temperature",
                                 "Leaf Wetness",
                                 "Soil Moisture"]

        self.ReportsTable.setHorizontalHeaderLabels(self.TableHeaderLabel)
        self.ReportsTable.horizontalHeader().setStyleSheet("color: #fff;")
        self.ReportsTable.horizontalHeader().adjustSize()
        self.ReportsTable.setStyleSheet("color: #fff; font-size: 16px; ")

        self.SerialData = ""

        # Initializing Database
        self.db = DatabaseFunctions()


        # Connect Buttons
        self.btnOpenHome.clicked.connect(lambda: self.HandleNavigation(0))
        self.btnOpenReports.clicked.connect(lambda: self.HandleNavigation(1))
        self.btnOpenSettings.clicked.connect(lambda: self.HandleNavigation(2))
        self.saveComPort.clicked.connect(self.SaveComPort)

        # Creating Timer
        self.progress_timer = QTimer()
        self.progress_timer.timeout.connect(self.updateProgress)
        self.progress_timer.start(1000)




    @pyqtSlot()
    def startReading(self):
        self.serial.open(QIODevice.ReadWrite)
        while self.serial.canReadLine():
            text = self.serial.readLine().data().decode()
            self.SerialData = text.rstrip('\r\n')


    @pyqtSlot()
    def updateProgress(self):
        line = self.SerialData
        print(len(line))
        line = line[0:len(line)].split(',')
        try:
            self.temperatureLabel.setText(str(line[0]))
            self.windLabel.setText(str(line[1]))
            self.windDirection.setText(str(line[2]))
            self.humidityLabel.setText(str(line[3]))
            self.rainfallLabel.setText(str(line[4]))
            self.solarLabel.setText(str(line[5]))
            self.soilMoistureLabel.setText(str(line[6]))
            self.barometerLabel.setText(str(line[7]))
            self.soilTempLabel.setText(str(line[8]))
            self.leafLabel.setText(str(line[9]))
            print(self.db.InsertData(line))
        except Exception as e:
            pass

    def HandleNavigation(self, pageNumber):
        self.MainNavigator.setCurrentIndex(pageNumber)
        if pageNumber == 0:
            self.startReading()
        if pageNumber == 1:
            self.UpdateReportsTable()


    def UpdateReportsTable(self):
        result = self.db.query.exec("select * from weather")
        row = 0

        while self.db.query.next():
            self.ReportsTable.insertRow(row)
            for index, item  in enumerate(self.TableHeaderLabel):
                label = item.casefold().replace(' ', "_")
                self.ReportsTable.setItem(row, index,
                                          QTableWidgetItem(self.db.query.value(f"{label}"))
                                          )

            row = row + 1

        self.ReportsTable.horizontalHeader().setStretchLastSection(True)
        self.ReportsTable.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch)
        self.db.CloseDatabase()

    def SaveComPort(self):
        com_port = self.comPortText.text()
        with open('config.json', 'w') as config:
            config.write('{"com_port": "'+com_port+'"}')


# SPLASH SCREEN
class SplashScreen(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_SplashScreen()
        self.ui.setupUi(self)
        self.ui.label_description = self.ui.label_4

        ## UI ==> INTERFACE CODES
        ########################################################################

        ## REMOVE TITLE BAR
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)


        ## DROP SHADOW EFFECT
        self.shadow = QGraphicsDropShadowEffect(self)
        self.shadow.setBlurRadius(20)
        self.shadow.setXOffset(0)
        self.shadow.setYOffset(0)
        self.shadow.setColor(QColor(0, 0, 0, 60))
        # self.ui.dropShadowFrame.setGraphicsEffect(self.shadow)

        ## QTIMER ==> START
        self.timer = QTimer()
        self.timer.timeout.connect(self.progress)
        # TIMER IN MILLISECONDS
        self.timer.start(30)

        # CHANGE DESCRIPTION

        # Initial Text
        self.ui.label_description.setText("<strong>LOADING</strong> CONFIGURATIONS")

        # Change Texts
        QTimer.singleShot(1500, lambda: self.ui.label_description.setText("<strong>LOADING</strong> DATABASE"))
        QTimer.singleShot(3000, lambda: self.ui.label_description.setText("<strong>LOADING</strong> USER INTERFACE"))


        ## SHOW ==> MAIN WINDOW
        ########################################################################
        self.show()
        ## ==> END ##

    ## ==> APP FUNCTIONS
    ########################################################################
    def progress(self):

        global counter

        # SET VALUE TO PROGRESS BAR
        self.ui.progressBar.setValue(counter)

        # CLOSE SPLASH SCREE AND OPEN APP
        if counter > 100:
            # STOP TIMER
            self.timer.stop()

            # SHOW MAIN WINDOW
            self.main = WeatherStation()
            self.main.show()

            # CLOSE SPLASH SCREEN
            self.close()

        # INCREASE COUNTER
        counter += 1



if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = SplashScreen()
    window.show()

    app.exec_()