import requests
from PyQt5 import QtSql
from PyQt5.QtSql import QSqlDatabase, QSqlQuery

class DatabaseFunctions():
    def __init__(self):
        super(DatabaseFunctions, self).__init__()
        self.SetupDatabase()

    def SetupDatabase(self):
        self.con = QSqlDatabase.addDatabase("QSQLITE")
        self.con.setDatabaseName("database.db")
        self.con.open()

        self.query = QSqlQuery()

        res = self.query.exec("""
            CREATE TABLE weather (
        id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL,
        wind_speed VARCHAR(40) NOT NULL,
        wind_direction VARCHAR(50),
        air_temperature VARCHAR(40) NOT NULL,
        humidity VARCHAR(20),
        rainfall VARCHAR(20),
        solar_radiation VARCHAR(20),
        barometric_pressure VARCHAR(20),
        soil_temperature VARCHAR(20),
        leaf_wetness VARCHAR(20),
        soil_moisture VARCHAR(20)
    ) """
        )
        print(res)
        return True

    def InsertData(self,values):
        if self.con.open():
            res = self.query.exec(f"insert into weather (wind_speed,"
                                                        f"wind_direction,"
                                                        f"air_temperature,"
                                                        f"humidity,"
                                                        f"rainfall,"
                                                        f"solar_radiation,"
                                                        f"barometric_pressure,"
                                                        f"soil_temperature,"
                                                        f"leaf_wetness,"
                                                        f"soil_moisture ) "
                                  f"values('{values[0]}', '{values[1]}', '{values[2]}', '{values[3]}','{values[4]}', '{values[5]}', '{values[6]}', '{values[7]}','{values[8]}', '{values[9]}')")

            print(res)
            if(res):
                url = "http://tgc.ac.in/CI/weather/savedata"
                data = {
                    "windSpeed": f"{values[0]}",
                    "windDirection": f"{values[1]}",
                    "airTemp": f"{values[2]}",
                    "humidity": f"{values[3]}",
                    "rainfall": f"{values[4]}",
                    "solarRadiation": f"{values[5]}",
                    "barometer": f"{values[6]}",
                    "soilTemp": f"{values[7]}",
                    "leafWetness": f"{values[8]}",
                    "soilMoisture": f"{values[9]}",
                }

                response = requests.post(url=url, data=data)
                print(response.status_code)
                return "Successfully Inserted Data"
            else:
                return False
        else:
            return False

    def ReadData(self):
        result = self.query.exec("select * from weather")
        if(result):
            print(self.query)
            return self.query
        else:
            return False

    def CloseDatabase(self):
        self.con.close()

if __name__ == "__main__":
    db = DatabaseFunctions()
    # db.SetupDatabase()
    # db.InsertData(['2','4','5','5','2','8','12','4','3','1','6'])
    db.ReadData()